package iomallach.gbot

import iomallach.gbot.GDocsBot.GDocsBot

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object Main extends App {
  val bot = new GDocsBot()
  val eol = bot.run()
  Await.result(eol, Duration.Inf)
}
