package iomallach.gbot.GDocsBot

import com.bot4s.telegram.future.{Polling, TelegramBot}
import com.bot4s.telegram.api.declarative.Commands
import cats.instances.future._
import cats.syntax.functor._
import com.bot4s.telegram.api.RequestHandler
import com.bot4s.telegram.clients.ScalajHttpClient

import scala.concurrent.Future

class GDocsBot extends TelegramBot
  with Polling with Commands[Future] {
  //TODO: read token from yaml
  override val client: RequestHandler[Future] = new ScalajHttpClient("961359944:AAErdeYWYacjd3IQKA-Wm29XR7AaRTqGWEc")

  onCommand("beer" | "beers") { implicit msg =>
    withArgs {
      case Seq(i) =>reply(s"Added ${i}").void
      case _ => reply("Invalid argument, argument must be numeric, i.e /beer 350").void
    }
  }
}
