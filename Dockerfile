FROM ubuntu:latest
RUN apt-get update && apt-get install -y apt-transport-https
RUN apt-get install -y default-jdk
WORKDIR /usr/src/app
COPY . .
RUN ./gradlew jar
CMD [ "java", "-jar", "./build/libs/gbot-1.0-SNAPSHOT.jar"]